//
//  ViewController.swift
//  CoreDataExample
//
//  Created by Abhishek Bansal on 09/11/18.
//  Copyright © 2018 Abhishek Bansal. All rights reserved.
//

// follow the three steps which is in bitbucket


import UIKit
import CoreData     //1. import core data

class ViewController: UIViewController {
    
    //MARK: Outlets
    
    @IBOutlet weak var nameTextBox: UITextField!
    
    
    
    
    
    //2. add mandotory core data variables
    var context:NSManagedObjectContext!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //3. intializing/setup the core data variables
        
        // Setup your CoreData variable
        // ----------------------------------------
        
        // 1. Mandatory - copy and paste this
        // Explanation: try to create/initalize the appDelegate variable.
        // If creation fails, then quit the app
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        
        // 2. Mandatory - initialize the context variable
        // This variable gives you access to the CoreData functions
        self.context = appDelegate.persistentContainer.viewContext
        
    }

    //MARK: Actions
    
    @IBAction func addButton(_ sender: Any) {
        print("Add button pressed")
        
        //1. get data from textbox
        let x = nameTextBox.text!
        
        //2. create "Person" object
        var p = Person(context: self.context)
        
        //3. set properties of person object
        // the properties here (name and age) comes from entity(database which we created)
        p.age = 40
        p.name = x
        
        //4. save the person object to database
        do {
            // Save the user to the database
            // (Send the INSERT to the database)
            try self.context.save()
            print("Saved the person to the database")
        }
        catch {
            print("Error while saving to the database")
        }
        
        //5. done
        
        
    }
    
    @IBAction func loadButton(_ sender: Any) {
        print("Load button pressed")
        
        
        // copy,paste and change the name to "Person" from bitbucket teacher's
        
        
        // This is the same as:  SELECT * FROM User
        let fetchRequest:NSFetchRequest<Person> = Person.fetchRequest()
        
        do {
            // Send the "SELECT *" to the database
            //  results = variable that stores any "rows" that come back from the db
            // Note: The database will send back an array of User objects
            // (this is why I explicilty cast results as [User]
            let results = try self.context.fetch(fetchRequest) as [Person]
            
            // Loop through the database results and output each "row" to the screen
            print("Number of items in database: \(results.count)")
            
            for x in results {
                print("Person Name: \(x.name)")
                print("Person Age: \(x.age)")
            }
        }
        catch {
            print("Error when fetching from database")
        }
        
        
    }
    
}

